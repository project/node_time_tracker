
node_time_tracker is a time tracking system per node type. You can set any type of node to have a time tracking block. This block has a couple of buttons to start and stop the clock. The clock keeps counting even if you close the browser window. Once you stop it, it will show you the total time you are spending on that node doing whatever you like to track (I'm using it to track the time I'm spending to solve my client's issues, which are cck node)

Once installed you need to allow the time tracker feature for at least one type of node on admin > content > types > 'your_type_of_content'

Set the visibility of the time tracker block on admin > build > block

This module depends on views to show a list of time tracker entries. It also depends on node relativity module

________NOTE_________________________________________________________________
Note that this is a module in a very early stage. It is actually working by showing the total tracked time per node but it lacks, yet, a fully functional tracking entries administration. I would like, also, to have some nice ajax features added. Please, use the feature request form at drupal.org to let me know your thoughts on the features you would like to see in this module.